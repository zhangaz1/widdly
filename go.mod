module gitlab.com/opennota/widdly

require (
	github.com/boltdb/bolt v1.3.1
	github.com/daaku/go.zipexe v1.0.1
	golang.org/x/crypto v0.0.0-20190513172903-22d7a77e9e5f
	golang.org/x/sys v0.0.0-20190516110030-61b9204099cb // indirect
)
